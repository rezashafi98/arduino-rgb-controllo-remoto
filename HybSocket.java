package com.example.bluetoothcolor;

import android.bluetooth.BluetoothSocket;
public class HybSocket
{
    private static BluetoothSocket hybridSocket;

    public static void setHybridSocket (BluetoothSocket s)
    {
        HybSocket.hybridSocket = s;
    }

    public static BluetoothSocket getHybridSocket()
    {
        return HybSocket.hybridSocket;
    }
}
