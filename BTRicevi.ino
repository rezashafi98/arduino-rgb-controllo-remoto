/* Riceve messaggi che vengono inviati via Bluetooth HC-05 da una app. 
*/

#include <SoftwareSerial.h>
#include <String.h>
const int RXPin = 2;  // da collegare su TX di HC05
const int TXPin = 3;  // da collegare su RX di HC05

const int ledred=9; //Pin LED Red
const int ledgreen=10;//Pin LED Green
const int ledblue=11; //Pin LED Blue

const int ritardo = 10;

//creo una nuova porta seriale via software
SoftwareSerial myBT = SoftwareSerial(RXPin, TXPin);
  
char msgRCVD;

int red;
int green;
int blue;
  
void setup()
{
  myBT.begin(38400);
  
  pinMode(RXPin, INPUT);
  pinMode(TXPin, OUTPUT);
  
  pinMode(ledred,OUTPUT);
  pinMode(ledgreen,OUTPUT);
  pinMode(ledblue,OUTPUT);
  
  Serial.begin(38400);
  
}

  
void loop() 
{
  String msg;
  while(myBT.available())
  {
    msgRCVD = myBT.read();
    msg = msgRCVD;
    Serial.print(msg.substring(0,1));  /*non riesco a far funzionare la substring ovvero non stampa correttamente*/
  }
  
  analogWrite(ledred, red); 
  //analogWrite(ledgreen, 255);
  //analogWrite(ledblue, 255);
  delay(ritardo); //delay                
}
