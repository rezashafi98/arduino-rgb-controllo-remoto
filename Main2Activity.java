package com.example.bluetoothcolor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class Main2Activity extends AppCompatActivity
{
    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket btSocket;
    OutputStreamWriter writer;
    LinearLayout linLayCerchio;
    SeekBar seekBarRed;
    SeekBar seekBarGreen;
    SeekBar seekBarBlue;
    Button btnClose;
    private int seekR, seekG, seekB;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        linLayCerchio = (LinearLayout) findViewById(R.id.linLayCerchio);
        seekBarRed = (SeekBar) findViewById(R.id.seekBarRed);
        seekBarGreen = (SeekBar) findViewById(R.id.seekBarGreen);
        seekBarBlue = (SeekBar) findViewById(R.id.seekBarBlue);
        btSocket = HybSocket.getHybridSocket();
        btnClose = (Button) findViewById(R.id.btnClose);

        seekBarRed.setOnSeekBarChangeListener(seekBarChangeListenerR);
        seekBarGreen.setOnSeekBarChangeListener(seekBarChangeListenerG);
        seekBarBlue.setOnSeekBarChangeListener(seekBarChangeListenerB);
        btnClose.setOnClickListener(btnCloseListener);
        sendValues();

    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerR =
            new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                {
                    seekR = progress;
                    color();
                    sendValues();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };
    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerG =
            new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                {
                    seekG = progress;
                    color();
                    sendValues();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {

                }
            };
    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerB =
            new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                {
                    seekB = progress;
                    color();
                    sendValues();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {

                }

            };

    private Button.OnClickListener btnCloseListener =
            new Button.OnClickListener()
            {
                @Override
                public void onClick(View arg0)
                {
                    onDestroy();
                }
            };

    public String getData()
    {
        String msg,r,g,b;
        r= Integer.toString(seekR);
        g= Integer.toString(seekG);
        b= Integer.toString(seekB);

        if(seekR==0)
        {
            r="000";
        }else if(seekR<10)
        {
            r= "00" + r;
        }else if(seekR>10&&seekR<100)
        {
            r = "0" + r;
        }
        /****************************/
        if(seekG==0)
        {
            g="000";
        }else if(seekG<10)
        {
            g= "00" + g;
        }else if(seekG>10&&seekG<100)
        {
            g = "0" + g;
        }
        /****************************/
        if(seekB==0)
        {
            b="000";
        }else if(seekB<10)
        {
            b= "00" + r;
        }else if(seekB>10&&seekB<100)
        {
            b = "0" + b;
        }

        msg = r+'-'+g+'-'+b;
        return msg;
    }
    public void sendValues()
    {
        String r= Integer.toString(seekBarRed.getProgress());
        String g= Integer.toString(seekBarGreen.getProgress());
        String b= Integer.toString(seekBarBlue.getProgress());
        try
        {
            writer = new OutputStreamWriter(btSocket.getOutputStream());
            writer.write(r+'-'+g+'-'+b+"\n");
            writer.flush();
        } catch (IOException e) {
            finish();
        }
    }
    public void color()
    {
        GradientDrawable my_shape = (GradientDrawable) linLayCerchio.getBackground();
        int color = Color.rgb(seekR, seekG, seekB);
        my_shape.setColor(color);
    }
}
